package com.khilman.kotlinfootballclub.utils

import com.khilman.kotlinfootballclub.network.InitRetrofit
import com.khilman.kotlinfootballclub.network.response.ResponseEvent
import com.khilman.kotlinfootballclub.network.response.ResponseTeam
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiRepsitoryTest {
    @Test
    fun getDetailEvent(){
        InitRetrofit().instance().requestDetailEvent("526919")
            .enqueue(object : Callback<ResponseEvent>{
                override fun onFailure(call: Call<ResponseEvent>, t: Throwable) {
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<ResponseEvent>, response: Response<ResponseEvent>) {
                    if (response.isSuccessful){
                        val events = response.body()?.events
                        Mockito.verify(events)?.size
                    }
                }

            })
    }

    @Test
    fun getSearchLeague(){
        InitRetrofit().instance().requestTeamsByLeague("A")
            .enqueue(object : Callback<ResponseTeam>{
                override fun onFailure(call: Call<ResponseTeam>, t: Throwable) {
                    t.printStackTrace()
                }

                override fun onResponse(call: Call<ResponseTeam>, response: Response<ResponseTeam>) {
                    if (response.isSuccessful){
                        val teams = response.body()?.teams
                        Mockito.verify(teams)?.size
                    }
                }

            })
    }
}