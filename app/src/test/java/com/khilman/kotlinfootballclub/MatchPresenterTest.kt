package com.khilman.kotlinfootballclub

import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchInteractor
import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchPresenter
import com.khilman.kotlinfootballclub.activities.home.MatchView
import com.khilman.kotlinfootballclub.utils.MyConts
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.timeout
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MatchPresenterTest {

    @Mock
    private lateinit var matchView: MatchView
    private lateinit var matchPresenter: MatchPresenter

    @Before
    fun setUpTest() {
        MockitoAnnotations.initMocks(this)
        matchPresenter = MatchPresenter(
            matchView,
            MatchInteractor()
        )
    }

    @Test
    fun getMatches(){
        matchPresenter.showListEvents("4328", MyConts.MATCH_PREV, null)

        verify(matchView, timeout(3000)).showMatchEvents(Mockito.anyList())
    }
}