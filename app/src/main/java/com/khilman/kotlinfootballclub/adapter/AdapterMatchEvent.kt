package com.khilman.kotlinfootballclub.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.network.response.EventsItem
import kotlinx.android.synthetic.main.item_event.view.*

class AdapterMatchEvent(val context: Context?, val events: List<EventsItem>, val listener: (EventsItem) -> Unit) :
    RecyclerView.Adapter<AdapterMatchEvent.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MyViewHolder = MyViewHolder(
        LayoutInflater.from(context).inflate(
            R.layout.item_event, parent, false
        )
    )

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val event = events[position]
        holder.bind(event)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(event: EventsItem) {
            itemView.tv_match_date.text = event.dateEvent
            itemView.tv_home_team.text = event.strHomeTeam
            itemView.tv_home_team_score.text = event.intHomeScore

            itemView.tv_away_team.text = event.strAwayTeam
            itemView.tv_away_team_score.text = event.intAwayScore

            itemView.setOnClickListener {
                listener(event)
            }
        }

    }
}