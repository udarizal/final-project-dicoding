package com.khilman.kotlinfootballclub.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchFragment
import com.khilman.kotlinfootballclub.utils.MyConts

class MatchPagerAdapter(fm: FragmentManager, val context: Context) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return MatchFragment.newInstance(MyConts.MATCH_PREV)
        } else {
            return MatchFragment.newInstance(MyConts.MATCH_NEXT)
        }
    }

    override fun getCount(): Int = 2
    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = context.getString(R.string.title_prev_match)
        } else {
            title = context.getString(R.string.title_next_match)
        }
        return title
    }
}