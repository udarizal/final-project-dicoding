package com.khilman.kotlinfootballclub.network.response

import com.google.gson.annotations.SerializedName

data class ResponseTeam(

	@field:SerializedName("teams")
	val teams: List<TeamsItem>? = null
)