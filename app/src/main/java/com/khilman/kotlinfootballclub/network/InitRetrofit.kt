package com.khilman.kotlinfootballclub.network

import com.khilman.kotlinfootballclub.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class InitRetrofit {
    fun init(): Retrofit = Retrofit.Builder().baseUrl("${BuildConfig.BASE_URL}api/v1/json/1/").addConverterFactory(
        GsonConverterFactory.create()).build()
    fun instance(): ApiServices = init().create(ApiServices::class.java)
}