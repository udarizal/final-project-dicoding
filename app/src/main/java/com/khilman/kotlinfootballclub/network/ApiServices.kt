package com.khilman.kotlinfootballclub.network

import com.khilman.kotlinfootballclub.network.response.ResponseEvent
import com.khilman.kotlinfootballclub.network.response.ResponseTeam
import com.khilman.kotlinfootballclub.network.response.leagues.ResponseLeague
import com.khilman.kotlinfootballclub.network.response.player.ResponsePlayer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {

    @GET("eventspastleague.php")
    fun requestPastEvents(
        @Query("id") idLeague: String
    ): Call<ResponseEvent>

    @GET("eventsnextleague.php")
    fun requestNextEvents(
        @Query("id") idLeague: String
    ): Call<ResponseEvent>

    @GET("lookupevent.php")
    fun requestDetailEvent(
        @Query("id") idEvent: String
    ): Call<ResponseEvent>

    @GET("lookupteam.php")
    fun requestDetailTeam(
        @Query("id") idTeam: String
    ): Call<ResponseTeam>

    @GET("lookup_all_players.php")
    fun requestDetailPlayer(
        @Query("id") idTeam: String
    ): Call<ResponsePlayer>


    @GET("search_all_teams.php")
    fun requestTeamsByLeague(
        @Query("l") league: String
    ): Call<ResponseTeam>

    @GET("all_leagues.php")
    fun requestLeagues(): Call<ResponseLeague>


}