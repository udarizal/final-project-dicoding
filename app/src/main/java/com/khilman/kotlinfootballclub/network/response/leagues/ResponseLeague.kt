package com.khilman.kotlinfootballclub.network.response.leagues

import com.google.gson.annotations.SerializedName

data class ResponseLeague(

	@field:SerializedName("leagues")
	val leagues: List<LeaguesItem>? = null
)