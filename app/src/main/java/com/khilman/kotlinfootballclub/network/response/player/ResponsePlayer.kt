package com.khilman.kotlinfootballclub.network.response.player

import com.google.gson.annotations.SerializedName

data class ResponsePlayer(

	@field:SerializedName("player")
	val player: List<PlayerItem?>? = null
)