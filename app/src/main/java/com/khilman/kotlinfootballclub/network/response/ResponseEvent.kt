package com.khilman.kotlinfootballclub.network.response

import com.google.gson.annotations.SerializedName

data class ResponseEvent(
	@field:SerializedName("events")
	val events: List<EventsItem>
)