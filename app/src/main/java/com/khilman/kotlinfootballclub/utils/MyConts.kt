package com.khilman.kotlinfootballclub.utils

class MyConts {
    companion object {
        const val CLUB_NAME = "CLUB_NAME"
        const val CLUB_IMAGE = "CLUB_IMAGE"
        const val CLUB_DESC = "CLUB_DESC"
        const val TEAM_HOME = "HOME"
        const val TEAM_AWAY = "AWAY"
        const val EVENT_ID = "EVENT_ID"
        const val MATCH_PREV = "PREV"
        const val MATCH_NEXT = "NEXT"
        const val MATCH_FAV = "FAVORITE"
        const val MATCH_DATA = "MATCH"
    }
}