package com.khilman.kotlinfootballclub.utils

import android.support.test.espresso.IdlingResource
import com.khilman.kotlinfootballclub.activities.home.MatchScheduleActivity

class ResourceIdlingResource(private val matchActivity: MatchScheduleActivity) : IdlingResource {
    private var callback: IdlingResource.ResourceCallback? = null

    override fun getName(): String {
        return javaClass.name
    }

    override fun isIdleNow(): Boolean {
        if (matchActivity.isMatchLoadFinished){
            callback?.onTransitionToIdle()
        }
        return matchActivity.isMatchLoadFinished
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.callback = callback
    }
}