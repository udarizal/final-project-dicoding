package com.khilman.kotlinfootballclub.utils

import java.text.SimpleDateFormat
import java.util.*

@SuppressWarnings("SimpleDateFormat")
fun toSimpleString(date: Date): String? = with(date ?: Date()) {
    SimpleDateFormat("EEE, dd MMM yyy").format(this)
}