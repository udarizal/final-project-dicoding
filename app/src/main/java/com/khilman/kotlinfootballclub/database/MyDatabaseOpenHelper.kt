package com.khilman.kotlinfootballclub.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.khilman.kotlinfootballclub.database.FavoriteTeam.Companion.TABLE_FAVORITE_TEAM
import com.khilman.kotlinfootballclub.database.MatchEvent.Companion.TABLE_FAVORITE_MATCH
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) :
    ManagedSQLiteOpenHelper(ctx, "FavoriteTeam.db", null, 3) {

    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null){
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return  instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //here you can create table
        db?.createTable(TABLE_FAVORITE_TEAM, true,
            FavoriteTeam.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavoriteTeam.TEAM_ID to TEXT + UNIQUE,
            FavoriteTeam.TEAM_NAME to TEXT,
            FavoriteTeam.TEAM_BADGE to TEXT)

        db?.createTable(TABLE_FAVORITE_MATCH, true,
            MatchEvent.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            MatchEvent.MATCH_ID to TEXT + UNIQUE,
            MatchEvent.MATCH_DATE to TEXT,
            MatchEvent.HOME_TEAM to TEXT,
            MatchEvent.HOME_SCORE to TEXT,
            MatchEvent.AWAY_TEAM to TEXT,
            MatchEvent.AWAY_SCORE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //Here you can drop table
        db?.dropTable(TABLE_FAVORITE_TEAM, true)
        db?.dropTable(TABLE_FAVORITE_MATCH, true)
    }

}

//Access property for Context
val Context.database : MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)
