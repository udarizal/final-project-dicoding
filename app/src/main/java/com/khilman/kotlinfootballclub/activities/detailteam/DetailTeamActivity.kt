package com.khilman.kotlinfootballclub.activities.detailteam

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.R.id.addToFavorite
import com.khilman.kotlinfootballclub.activities.detailteam.adapter.AdapterTabTeam
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.fragment_overview.view.*
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast

class DetailTeamActivity : AppCompatActivity(), DetailTeamView {

    private lateinit var teamId: String
    private val presenter = DetailTeamPresenter(this, DetailTeamInteractor())
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var dataTeam: TeamsItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        teamId = intent.getStringExtra("TEAM_ID")

        view_pager.adapter = AdapterTabTeam(teamId, supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)

        presenter.getDetails(teamId)
        this.favoriteState()
    }

    override fun showLoading() {
        progressBar?.show()
    }

    override fun hideLoading() {
        progressBar?.hide()
    }

    override fun showMessage(message: String) {
        toast(message)
    }


    override fun showOverview(teamDetail: TeamsItem) {
        dataTeam = teamDetail
        supportActionBar?.title = teamDetail.strTeam
        tv_team_name.text = teamDetail.strTeam
        tv_formed_year.text = teamDetail.intFormedYear
        tv_stadium.text = teamDetail.strStadium
        Picasso.with(this).load(teamDetail.strTeamBadge).into(iv_team_badge)

    }

    override fun showTeamList(teamPlayers: List<PlayerItem?>) {

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_match_menu, menu)
        menuItem = menu
        this.setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.addToFavorite-> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        presenter.addToFavorite(this, dataTeam)
    }
    private fun removeFromFavorite() {
        presenter.removeFromFavorite(this, teamId)
    }
    private fun favoriteState() {
        isFavorite = presenter.isFavorite(this, teamId)
    }

    private fun setFavorite() {
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_on)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_off)
        }
    }
}
