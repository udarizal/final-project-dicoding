package com.khilman.kotlinfootballclub.activities.detailevent

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.database.MatchEvent
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.utils.MyConts
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_event.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.toast

class DetailEventActivity : AppCompatActivity(), DetailEventView {

    private var presenter = DetailEventPresenter(this, DetailEventInteractor())
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var eventId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_event)

        eventId = intent.getStringExtra(MyConts.EVENT_ID)
        presenter.showEvents(eventId)
        this.favoriteState()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_match_menu, menu)
        menuItem = menu
        this.setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.addToFavorite-> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun showLoading() {
        progressBar?.show()
    }

    override fun hideLoading() {
        progressBar?.hide()
    }

    override fun showMessage(message: String) {
        main_container.snackbar(message)
    }

    override fun showDetailEvent(events: List<EventsItem>) {
        if (events.isNotEmpty()){
            this.setDataEventToView(dataEvent = events.get(0))
        }
    }

    override fun showDetailAwayTeam(team: TeamsItem) {
        runOnUiThread {
            Picasso.with(this).load(team.strTeamBadge).into(logo_away)
        }
    }

    override fun showDetailHomeTeam(team: TeamsItem) {
        runOnUiThread {
            Picasso.with(this).load(team.strTeamBadge).into(logo_home)
        }
    }

    private lateinit var dataEvent: EventsItem
    fun setDataEventToView(dataEvent: EventsItem){
        //TODO: Get Logo
        presenter.showDetailTeam(dataEvent.idHomeTeam.toString(), MyConts.TEAM_HOME)
        presenter.showDetailTeam(dataEvent.idAwayTeam.toString(), MyConts.TEAM_AWAY)
        this.dataEvent = dataEvent

        //TODO: Set to Home Team
        date_match.text = dataEvent?.dateEvent
        club_home.text = dataEvent?.strHomeTeam
        formation_home.text = dataEvent?.strHomeFormation
        score_home.text = dataEvent?.intHomeScore
        goal_home.text = dataEvent?.strHomeGoalDetails
        shots_home.text = dataEvent?.intHomeShots
        goalkeeper_home.text = dataEvent?.strHomeLineupGoalkeeper
        defense_home.text = dataEvent?.strHomeLineupDefense
        midfield_home.text = dataEvent?.strHomeLineupMidfield
        forward_home.text = dataEvent?.strAwayLineupForward
        subtitutes_home.text = dataEvent?.strHomeLineupSubstitutes
        yellowcard_home.text = dataEvent?.strHomeYellowCards
        redcard_home.text = dataEvent?.strHomeRedCards

        //TODO: Set to Home Away
        club_away.text = dataEvent?.strAwayTeam
        formation_away.text = dataEvent?.strAwayFormation
        score_away.text = dataEvent?.intAwayScore
        goal_away.text = dataEvent?.strAwayGoalDetails
        shots_away.text = dataEvent?.intAwayShots
        goalkeeper_away.text = dataEvent?.strAwayLineupGoalkeeper
        defense_away.text = dataEvent?.strAwayLineupDefense
        midfield_away.text = dataEvent?.strAwayLineupMidfield
        forward_away.text = dataEvent?.strAwayLineupForward
        subtitutes_away.text = dataEvent?.strAwayLineupSubstitutes
        yellowcard_away.text = dataEvent?.strAwayYellowCards
        redcard_away.text = dataEvent?.strAwayRedCards
    }

    private fun addToFavorite() {
        presenter.addToFavorite(this, dataEvent)
    }
    private fun removeFromFavorite() {
        presenter.removeFromFavorite(this, eventId)
    }
    private fun favoriteState() {
        isFavorite = presenter.isFavorite(this, eventId)
    }

    private fun setFavorite() {
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_on)
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_off)
        }
    }
}
