package com.khilman.kotlinfootballclub.activities.home

import com.khilman.kotlinfootballclub.network.response.EventsItem

interface MatchView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String)
    fun showMatchEvents(events: List<EventsItem>)
}