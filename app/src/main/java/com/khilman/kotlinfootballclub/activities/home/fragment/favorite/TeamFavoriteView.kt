package com.khilman.kotlinfootballclub.activities.home.fragment.favorite

import com.khilman.kotlinfootballclub.network.response.TeamsItem

interface TeamFavoriteView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String)
    fun showEvents(events: List<TeamsItem>)
}