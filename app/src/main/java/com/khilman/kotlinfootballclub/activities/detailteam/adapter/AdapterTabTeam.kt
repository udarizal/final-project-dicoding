package com.khilman.kotlinfootballclub.activities.detailteam.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.khilman.kotlinfootballclub.activities.detailteam.fragment.OverviewFragment
import com.khilman.kotlinfootballclub.activities.detailteam.fragment.PlayerFragment

class AdapterTabTeam(val teamId: String, fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        if (position == 0) {
            fragment = OverviewFragment.newInstance(teamId)
        } else if (position == 1) {
            fragment = PlayerFragment.newInstance(teamId)
        }
        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = "Overview"
        } else if (position == 1) {
            title = "Player"
        }
        return title
    }
}