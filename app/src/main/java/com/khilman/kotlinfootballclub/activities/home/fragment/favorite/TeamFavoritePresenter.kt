package com.khilman.kotlinfootballclub.activities.home.fragment.favorite

import android.content.Context
import com.khilman.kotlinfootballclub.database.FavoriteTeam
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class TeamFavoritePresenter(val view: TeamFavoriteView) {

    fun getFavTeams(context: Context?){
        context?.database?.use {
            val result = select(FavoriteTeam.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<FavoriteTeam>())

            val teamFavorites: MutableList<TeamsItem> = ArrayList()

            favorite.forEachByIndex {
                val event = TeamsItem(idTeam = it.teamId,
                    strTeam = it.teamName,
                    strTeamBadge = it.teamBadge)
                teamFavorites.add(event)
            }

            view.showEvents(teamFavorites)
            view.hideLoading()
        }
    }
}