package com.khilman.kotlinfootballclub.activities.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.fragment.favorite.FavoriteFragment
import com.khilman.kotlinfootballclub.activities.home.fragment.match.HomeMatchFragment
import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchFragment
import com.khilman.kotlinfootballclub.activities.home.fragment.team.TeamFragment
import kotlinx.android.synthetic.main.activity_match_schedule.*

class MatchScheduleActivity : AppCompatActivity(), MatchFragment.OnFragmentInteractionListener {

    var isMatchLoadFinished: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_schedule)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.tabMatch -> {
                    loadFragment(savedInstanceState, "MATCH")
                }
                R.id.tabTeam -> {
                    loadFragment(savedInstanceState, "TEAM")
                }
                R.id.tabFav -> {
                    loadFragment(savedInstanceState, "FAV")
                }
            }
            true
        }
        bottom_navigation.selectedItemId = R.id.tabMatch
    }

    private fun loadFragment(savedInstanceState: Bundle?, whichFragment: String) {
        if (savedInstanceState == null){
            when(whichFragment){
                "MATCH" -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.main_container,
                            HomeMatchFragment(), HomeMatchFragment::class.java.simpleName)
                        .commit()
                }

                "TEAM" -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.main_container,
                            TeamFragment(), TeamFragment::class.java.simpleName)
                        .commit()
                }

                "FAV" -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.main_container,
                            FavoriteFragment(), FavoriteFragment::class.java.simpleName)
                        .commit()
                }
            }

        }
    }

    override fun matchLoadHasFinished() {
        isMatchLoadFinished = true
    }
}
//private fun loadTeamMatchSchedule(savedInstanceState: Bundle?, whichMatch: String) {
//    if (savedInstanceState == null){
//        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.main_container, MatchFragment.newInstance(whichMatch), MatchFragment::class.java.simpleName)
//            .commit()
//    }
//}
