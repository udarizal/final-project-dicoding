package com.khilman.kotlinfootballclub.activities.detailteam.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamInteractor
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamPresenter
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamView
import com.khilman.kotlinfootballclub.activities.detailteam.adapter.PlayerAdapter
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import kotlinx.android.synthetic.main.fragment_player.view.*
import org.jetbrains.anko.support.v4.toast

class PlayerFragment : Fragment(), DetailTeamView {

    companion object {
        fun newInstance(teamId: String): PlayerFragment {
            val fragment = PlayerFragment()
            val args = Bundle()
            args.putString("TEAM_ID", teamId)
            fragment.arguments = args
            return fragment
        }
    }

    private var teamId: String? = null
    private val presenter = DetailTeamPresenter(this, DetailTeamInteractor())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bundle = this.arguments
        val v = inflater.inflate(R.layout.fragment_player, container, false)
        teamId = bundle?.getString("TEAM_ID")

        //TODO: Get team details
        presenter.getPlayers(teamId.toString())
        return v
    }
    override fun showLoading() {
        view?.progressBar?.show()
    }

    override fun hideLoading() {
        view?.progressBar?.hide()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showOverview(teamDetail: TeamsItem) {

    }

    override fun showTeamList(teamPlayers: List<PlayerItem?>) {
        view?.recycler_view_player?.adapter = PlayerAdapter(context, teamPlayers)
        view?.recycler_view_player?.layoutManager = LinearLayoutManager(context)
    }

}
