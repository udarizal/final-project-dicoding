package com.khilman.kotlinfootballclub.activities.home.fragment.team


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.adapter.TeamAdapter
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.leagues.LeaguesItem
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import kotlinx.android.synthetic.main.fragment_team.view.*
import org.jetbrains.anko.support.v4.toast

class TeamFragment : Fragment(), TeamView {


    val presenter = TeamPresenter(this, TeamInteractor())
    val leagues: ArrayList<LeaguesItem> = ArrayList()

    companion object {
        fun newInstance(whichTeam: String): TeamFragment {
            val fragment = TeamFragment()
            val args = Bundle()
            args.putString("WHICH_TEAM", whichTeam)
            fragment.arguments = args
            return fragment
        }
    }

    private var whichTeam: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = this.arguments


        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_team, container, false)

        //get which team
        whichTeam = bundle?.getString("WHICH_TEAM")

        if (whichTeam.equals("TEAM_FAV")) {
            v.topPanel.hide()
            presenter.getFavTeams(context)
        } else {
            //get all leagues
            presenter.getLeagues()
            //search
            v?.etSearchBox?.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(string: CharSequence, start: Int, before: Int, count: Int) {
                    //filter
                    val teams = dataTeams.filter {
                        it.strTeam!!.contains(string, ignoreCase = true)
                    }
                    view?.recycler_view_team?.adapter = TeamAdapter(context, teams)

                }

            })
        }

        return v
    }

    override fun showLoading() {
        view?.progressBar?.show()
    }

    override fun hideLoading() {
        view?.progressBar?.hide()
    }

    override fun showLeagues(dataLeagues: List<LeaguesItem>) {
        this.leagues.addAll(dataLeagues)
        val adapter: ArrayAdapter<LeaguesItem> = ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, leagues)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        view?.spinner_team?.adapter = adapter
        view?.spinner_team?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter.getTeamsByLeague(leagues[position].strLeague.toString())
            }

        }
    }

    private var dataTeams: MutableList<TeamsItem> = mutableListOf()
    override fun showTeams(teams: List<TeamsItem>) {
        dataTeams.clear()
        dataTeams.addAll(teams)

        view?.recycler_view_team?.adapter = TeamAdapter(context, teams)
        view?.recycler_view_team?.layoutManager = LinearLayoutManager(context)
    }
    override fun showMessage(message: String) {
        toast(message)
    }

}
