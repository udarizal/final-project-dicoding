package com.khilman.kotlinfootballclub.activities.home.fragment.match

import android.content.Context
import com.khilman.kotlinfootballclub.activities.home.MatchView
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.utils.MyConts

class MatchPresenter(val view: MatchView, val interactor: MatchInteractor) :
    MatchInteractor.OnGetEventFinished {
    override fun onError(message: String) {
        view?.apply {
            showMessage(message)
            hideLoading()
        }
    }

    override fun onSuccess(events: List<EventsItem>) {
        view?.apply {
            showMatchEvents(events)
            hideLoading()
        }
    }

    fun showListEvents(leagueId: String, whichEvent: String, context: Context?) {
        if (leagueId.isNotEmpty()) {
            view.showLoading()

            if (whichEvent.equals(MyConts.MATCH_FAV)){
                interactor.getEventFavorite(context, this)
            } else {
                interactor.getEvent(leagueId, whichEvent, this)
            }
        }
    }

}