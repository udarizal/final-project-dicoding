package com.khilman.kotlinfootballclub.activities.home.fragment.favorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.adapter.AdapterTabFav
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import kotlinx.android.synthetic.main.fragment_favorite.view.*
import org.jetbrains.anko.support.v4.toast

class FavoriteFragment : Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_favorite, container, false)
        v.view_pager.adapter = AdapterTabFav(childFragmentManager)
        v.tab_layout.setupWithViewPager(v.view_pager)


        return v
    }





}
