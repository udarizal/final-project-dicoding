package com.khilman.kotlinfootballclub.activities.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamActivity
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.team_item_list.view.*
import org.jetbrains.anko.startActivity

class TeamAdapter(val context: Context?, val teams: List<TeamsItem>) : RecyclerView.Adapter<TeamAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): TeamAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.team_item_list, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamAdapter.ViewHolder, position: Int) {
        val data = teams[position]
        holder.bind(data)
        holder.itemView.setOnClickListener {
            context?.startActivity<DetailTeamActivity>("TEAM_ID" to data.idTeam.toString())
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: TeamsItem) {
            itemView.tvClubName.text = data.strTeam
            Picasso.with(context).load(data.strTeamBadge).into(itemView.ivClubImage)
        }
    }
}