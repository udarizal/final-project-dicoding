package com.khilman.kotlinfootballclub.activities.home.fragment.team

import android.content.Context
import com.khilman.kotlinfootballclub.database.FavoriteTeam
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.leagues.LeaguesItem
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class TeamPresenter(val view: TeamView, val interactor: TeamInteractor) : TeamInteractor.OnRequestApiFinished {


    override fun onError(message: String) {
        view?.apply {
            hideLoading()
            showMessage(message)
        }
    }

    override fun onSuccessLeague(leagues: List<LeaguesItem>) {
        view?.apply {
            hideLoading()
            showLeagues(leagues)
        }
    }
    override fun onSuccessTeams(teams: List<TeamsItem>) {
        view?.apply {
            hideLoading()
            showTeams(teams)
        }
    }

    fun getLeagues(){
        view.showLoading()
        interactor.getLeagues(this)
    }
    fun getTeamsByLeague(leagueName: String){
        view.showLoading()
        interactor.getTeamsInLeague(leagueName, this)
    }

    fun getFavTeams(context: Context?){
        view.hideLoading()
        context?.database?.use {
            val result = select(FavoriteTeam.TABLE_FAVORITE_TEAM)
            val favorite = result.parseList(classParser<FavoriteTeam>())

            val teamFavorites: MutableList<TeamsItem> = ArrayList()

            favorite.forEachByIndex {
                val event = TeamsItem(idTeam = it.teamId,
                    strTeam = it.teamName,
                    strTeamBadge = it.teamBadge)
                teamFavorites.add(event)
            }

            view.showTeams(teamFavorites)
        }
    }

}