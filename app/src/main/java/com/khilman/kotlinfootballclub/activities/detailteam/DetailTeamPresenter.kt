package com.khilman.kotlinfootballclub.activities.detailteam

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.khilman.kotlinfootballclub.database.FavoriteTeam
import com.khilman.kotlinfootballclub.database.MatchEvent
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailTeamPresenter(val view: DetailTeamView, val interactor: DetailTeamInteractor) : DetailTeamInteractor.OnApiRequestFinished {
    override fun onSuccessTeamPlayer(teamPlayers: List<PlayerItem?>) {
        view?.apply {
            hideLoading()
            showTeamList(teamPlayers)
        }
    }

    override fun onError(error: String) {
        view?.apply {
            hideLoading()
            showMessage(error)
        }
    }

    override fun onSuccessDetailTeam(teamDetail: TeamsItem) {
        view?.apply {
            hideLoading()
            showOverview(teamDetail)
        }
    }

    fun getDetails(teamId: String){
        view?.showLoading()
        interactor.getTeamDetail(teamId, this)
    }

    fun getPlayers(teamId: String){
        view?.showLoading()
        interactor.getTeamPlayer(teamId, this)
    }

    fun addToFavorite(
        context: Context,
        dataTeam: TeamsItem
    ){
        try {
            context?.database.use {
                insert(
                    FavoriteTeam.TABLE_FAVORITE_TEAM, FavoriteTeam.TEAM_ID to dataTeam.idTeam.toString(),
                    FavoriteTeam.TEAM_NAME to dataTeam.strTeam,
                    FavoriteTeam.TEAM_BADGE to dataTeam.strTeamBadge
                )
            }
            view?.showMessage("Ditambahkan ke favorite")

        } catch (e: SQLiteConstraintException){
            view?.showMessage(e.localizedMessage)
        }
    }

    fun removeFromFavorite(
        context: Context,
        idTeam: String
    ){
        try {
            context?.database.use {
                delete(FavoriteTeam.TABLE_FAVORITE_TEAM, "(${FavoriteTeam.TEAM_ID} = {id})", "id" to idTeam)
            }
            view?.showMessage("Favorite Team dihapus")
        } catch (e: SQLiteConstraintException) {
            view?.showMessage(e.localizedMessage)
        }
    }

    fun isFavorite (
        context: Context,
        idTeam: String
    ): Boolean {
        var isFavorite = false
        context?.database.use {
            val result = select(FavoriteTeam.TABLE_FAVORITE_TEAM)
                .whereArgs("(${FavoriteTeam.TEAM_ID} = {id})", "id" to idTeam)
            val favorite = result.parseList(classParser<FavoriteTeam>())
            isFavorite = favorite.isNotEmpty()
        }
        return isFavorite
    }

}