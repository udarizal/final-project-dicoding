package com.khilman.kotlinfootballclub.activities.home.fragment.match


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.detailevent.DetailEventActivity
import com.khilman.kotlinfootballclub.activities.home.MatchScheduleActivity
import com.khilman.kotlinfootballclub.activities.home.MatchView
import com.khilman.kotlinfootballclub.adapter.AdapterMatchEvent
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.utils.MyConts
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import kotlinx.android.synthetic.main.fragment_match.view.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class MatchFragment : Fragment(), MatchView {

    interface OnFragmentInteractionListener {
        fun matchLoadHasFinished()
    }

    private lateinit var onFragmentInteractionListener: OnFragmentInteractionListener
    private var presenter = MatchPresenter(
        this,
        MatchInteractor()
    )
    private var events: MutableList<EventsItem> = mutableListOf()
    private lateinit var adapter: AdapterMatchEvent
    private val leagueId = "4328"
    private var whichMatch: String? = null

    companion object {
        fun newInstance(whichMatch: String): MatchFragment {
            val fragment = MatchFragment()
            val args = Bundle()
            args.putString(MyConts.MATCH_DATA, whichMatch)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is MatchScheduleActivity){
            onFragmentInteractionListener = context
        } else {
            throw Throwable("Must implement OnFragmentInteractionListener")
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bundle = this.arguments
        val view = inflater.inflate(R.layout.fragment_match, container, false)

        //adapter list
        adapter = AdapterMatchEvent(context, events) {
            //on click event
            startActivity<DetailEventActivity>(MyConts.EVENT_ID to it.idEvent)
        }
        //recycler view
        view?.recyclerView?.layoutManager = LinearLayoutManager(context)
        view?.recyclerView?.adapter = adapter

        //get match event
        whichMatch = bundle?.getString(MyConts.MATCH_DATA)
        whichMatch?.let {
            presenter.showListEvents(leagueId, it, context)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        whichMatch?.let {
            presenter.showListEvents(leagueId, it, context)
        }
    }
    override fun showLoading() {
        view?.progressBar?.show()
    }

    override fun hideLoading() {
        view?.progressBar?.hide()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showMatchEvents(events: List<EventsItem>) {
        print(events)
        this.events.clear()
        this.events.addAll(events)

        adapter.notifyDataSetChanged()
        Log.d("Data event", events.toString())

        onFragmentInteractionListener.matchLoadHasFinished()
    }
}
