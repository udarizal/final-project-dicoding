package com.khilman.kotlinfootballclub.activities.home.fragment.match

import android.content.Context
import com.khilman.kotlinfootballclub.database.MatchEvent
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.InitRetrofit
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.network.response.ResponseEvent
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MatchInteractor {
    interface OnGetEventFinished {
        fun onError(message: String)
        fun onSuccess(events: List<EventsItem>)
    }

    val retrofit = InitRetrofit().instance()

    fun getEvent(leagueId: String, whichEvent: String, listener: OnGetEventFinished) {
        val api: Any
        api = if (whichEvent == "PREV") {
            retrofit.requestPastEvents(idLeague = leagueId)
        } else {
            retrofit.requestNextEvents(idLeague = leagueId)
        }

        api.enqueue(object : Callback<ResponseEvent> {
            override fun onFailure(call: Call<ResponseEvent>, t: Throwable) {
                t.printStackTrace() //print to log
                listener.onError("Request Failed : ${t.message})")
            }

            override fun onResponse(call: Call<ResponseEvent>, response: Response<ResponseEvent>) {
                if (response.isSuccessful) {
                    response.body()?.events?.let {
                        listener.onSuccess(it)
                    }
                } else {
                    listener.onError("Error : ${response.code()} ${response.message()}")
                }
            }

        })
    }

    fun getEventFavorite(context: Context?, listener: OnGetEventFinished){
        context?.database?.use {
            val result = select(MatchEvent.TABLE_FAVORITE_MATCH)
            val favorite = result.parseList(classParser<MatchEvent>())

            val matchFavorites: MutableList<EventsItem> = ArrayList()

            favorite.forEachByIndex {
                val event = EventsItem(idEvent = it.matchId,
                                        strHomeTeam = it.homeTeam,
                                        intHomeScore = it.homeScore,
                                        strAwayTeam = it.awayTeam,
                                        intAwayScore = it.awayScore,
                                        dateEvent = it.matchDate)
                matchFavorites.add(event)
            }

            listener.onSuccess(matchFavorites)
        }

    }
}