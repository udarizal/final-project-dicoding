package com.khilman.kotlinfootballclub.activities.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.khilman.kotlinfootballclub.activities.home.fragment.favorite.TeamFavoriteFragment
import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchFragment
import com.khilman.kotlinfootballclub.activities.home.fragment.team.TeamFragment
import com.khilman.kotlinfootballclub.utils.MyConts

class AdapterTabFav(fm: FragmentManager)  : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        if (position == 0) {
            fragment = MatchFragment.newInstance(MyConts.MATCH_FAV)

        } else if (position == 1) {
            fragment = TeamFavoriteFragment()

        }
        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = "Match"
        } else if (position == 1) {
            title = "Team"
        }
        return title
    }
}