package com.khilman.kotlinfootballclub.activities.home.fragment.match


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.adapter.AdapterTabMatch
import kotlinx.android.synthetic.main.fragment_home_match.view.*

class HomeMatchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_home_match, container, false)
        v.view_pager.adapter = AdapterTabMatch(childFragmentManager)
        v.tab_layout.setupWithViewPager(v.view_pager)
        return v
    }


}
