package com.khilman.kotlinfootballclub.activities.detailteam.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamInteractor
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamPresenter
import com.khilman.kotlinfootballclub.activities.detailteam.DetailTeamView
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import com.khilman.kotlinfootballclub.utils.hide
import com.khilman.kotlinfootballclub.utils.show
import org.jetbrains.anko.support.v4.toast
import kotlinx.android.synthetic.main.fragment_overview.view.*

class OverviewFragment : Fragment(), DetailTeamView {

    companion object {
        fun newInstance(teamId: String): OverviewFragment {
            val fragment = OverviewFragment()
            val args = Bundle()
            args.putString("TEAM_ID", teamId)
            fragment.arguments = args
            return fragment
        }
    }

    private var teamId: String? = null
    private val presenter = DetailTeamPresenter(this, DetailTeamInteractor())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bundle = this.arguments
        val v = inflater.inflate(R.layout.fragment_overview, container, false)
        teamId = bundle?.getString("TEAM_ID")

        //TODO: Get team details
        presenter.getDetails(teamId.toString())
        return v
    }

    override fun showLoading() {
        view?.progressBar?.show()
    }

    override fun hideLoading() {
        view?.progressBar?.hide()
    }

    override fun showMessage(message: String) {
        toast(message)
    }

    override fun showOverview(teamDetail: TeamsItem) {
        view?.team_overview?.text = teamDetail.strDescriptionEN

    }

    override fun showTeamList(teamPlayers: List<PlayerItem?>) {

    }

}
