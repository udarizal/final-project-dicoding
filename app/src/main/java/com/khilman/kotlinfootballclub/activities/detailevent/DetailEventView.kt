package com.khilman.kotlinfootballclub.activities.detailevent

import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.network.response.TeamsItem

interface DetailEventView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String)
    fun showDetailEvent(events: List<EventsItem>)
    fun showDetailAwayTeam(team: TeamsItem)
    fun showDetailHomeTeam(team: TeamsItem)
}