package com.khilman.kotlinfootballclub.activities.detailteam

import com.khilman.kotlinfootballclub.network.InitRetrofit
import com.khilman.kotlinfootballclub.network.response.ResponseTeam
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import com.khilman.kotlinfootballclub.network.response.player.ResponsePlayer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailTeamInteractor {
    interface OnApiRequestFinished {
        fun onError(error: String)
        fun onSuccessDetailTeam(teamDetail: TeamsItem)
        fun onSuccessTeamPlayer(teamPlayers: List<PlayerItem?>)
    }

    fun getTeamDetail(teamId: String, listener: OnApiRequestFinished){
        InitRetrofit().instance().requestDetailTeam(teamId)
            .enqueue(object : Callback<ResponseTeam>{
                override fun onFailure(call: Call<ResponseTeam>, t: Throwable) {
                    t.printStackTrace()
                    listener.onError("Failed : ${t.message}")
                }

                override fun onResponse(call: Call<ResponseTeam>, response: Response<ResponseTeam>) {
                    if (response.isSuccessful){
                        response.body()?.teams?.let {
                            val teamDetail = it[0]
                            listener.onSuccessDetailTeam(teamDetail)
                        }
                    }
                }

            })
    }
    fun getTeamPlayer(teamId: String, listener: OnApiRequestFinished){
        InitRetrofit().instance().requestDetailPlayer(teamId)
            .enqueue(object : Callback<ResponsePlayer>{
                override fun onFailure(call: Call<ResponsePlayer>, t: Throwable) {
                    t.printStackTrace()
                    listener.onError("Failed : ${t.message}")
                }

                override fun onResponse(call: Call<ResponsePlayer>, response: Response<ResponsePlayer>) {
                    if (response.isSuccessful){
                        response.toString()
                        response.body()?.player?.let {
                            listener.onSuccessTeamPlayer(it)
                        }
                    }
                }

            })
    }
}