package com.khilman.kotlinfootballclub.activities.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.khilman.kotlinfootballclub.activities.home.fragment.match.MatchFragment
import com.khilman.kotlinfootballclub.utils.MyConts

class AdapterTabMatch(fm: FragmentManager)  : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        var fragment: Fragment? = null
        if (position == 0) {
            fragment = MatchFragment.newInstance(MyConts.MATCH_PREV)
        } else if (position == 1) {
            fragment = MatchFragment.newInstance(MyConts.MATCH_NEXT)
        }
        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = "Last Match"
        } else if (position == 1) {
            title = "Next Match"
        }
        return title
    }
}