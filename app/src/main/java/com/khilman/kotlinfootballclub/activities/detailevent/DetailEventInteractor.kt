package com.khilman.kotlinfootballclub.activities.detailevent

import com.khilman.kotlinfootballclub.network.InitRetrofit
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.network.response.ResponseEvent
import com.khilman.kotlinfootballclub.network.response.ResponseTeam
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailEventInteractor {
    interface OnGetEventFinished {
        fun onError(message: String)
        fun onSuccess(events: List<EventsItem>)
        fun onTeamFound(teams: TeamsItem, whichTeam: String)
    }

    val retrofit = InitRetrofit().instance()
    fun getDetailEvent(eventId: String, listener: OnGetEventFinished) {
        val api = retrofit.requestDetailEvent(eventId)
        api.enqueue(object : Callback<ResponseEvent> {
            override fun onFailure(call: Call<ResponseEvent>, t: Throwable) {
                t.printStackTrace() //print to log
                listener.onError("Request Failed : ${t.message})")
            }

            override fun onResponse(call: Call<ResponseEvent>, response: Response<ResponseEvent>) {
                if (response.isSuccessful) {
                    response.body()?.events?.let {
                        listener.onSuccess(it)
                    }
                } else {
                    listener.onError("Error : ${response.code()} ${response.message()}")
                }
            }

        })
    }

    fun getDetailTeam(teamId: String, whichTeam: String, listener: OnGetEventFinished) {
        val api = retrofit.requestDetailTeam(teamId)
        api.enqueue(object : Callback<ResponseTeam> {
            override fun onFailure(call: Call<ResponseTeam>, t: Throwable) {
                t.printStackTrace() //print to log
                listener.onError("Request Failed : ${t.message})")
            }

            override fun onResponse(call: Call<ResponseTeam>, response: Response<ResponseTeam>) {
                if (response.isSuccessful) {
                    val teams = response.body()?.teams
                    teams?.get(0)?.let {
                        listener.onTeamFound(it, whichTeam)
                    }
                } else {
                    listener.onError("Error : ${response.code()} ${response.message()}")
                }
            }

        })
    }
}