package com.khilman.kotlinfootballclub.activities.home.fragment.team

import com.khilman.kotlinfootballclub.network.InitRetrofit
import com.khilman.kotlinfootballclub.network.response.ResponseTeam
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.leagues.LeaguesItem
import com.khilman.kotlinfootballclub.network.response.leagues.ResponseLeague
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TeamInteractor {

    interface OnRequestApiFinished{
        fun onError(message: String)
        fun onSuccessLeague(leagues: List<LeaguesItem>)
        fun onSuccessTeams(teams: List<TeamsItem>)
    }

    fun getLeagues(listener: OnRequestApiFinished){
        InitRetrofit().instance().requestLeagues()
            .enqueue(object : Callback<ResponseLeague>{
                override fun onFailure(call: Call<ResponseLeague>, t: Throwable) {

                }

                override fun onResponse(call: Call<ResponseLeague>, response: Response<ResponseLeague>) {
                    if (response.isSuccessful){
                        val leagues = response.body()?.leagues
                        leagues?.let {
                            listener.onSuccessLeague(leagues)
                        }
                    }
                }

            })
    }

    fun getTeamsInLeague(leagueName: String, listener: OnRequestApiFinished){
        InitRetrofit().instance().requestTeamsByLeague(leagueName)
            .enqueue(object : Callback<ResponseTeam>{
                override fun onFailure(call: Call<ResponseTeam>, t: Throwable) {
                    t.printStackTrace()
                    listener.onError("Failed : ${t.message}")
                }

                override fun onResponse(call: Call<ResponseTeam>, response: Response<ResponseTeam>) {
                    if (response.isSuccessful){
                        val teams = response.body()?.teams
                        teams?.let {
                            listener.onSuccessTeams(teams)
                        }
                    }
                }

            })
    }
}