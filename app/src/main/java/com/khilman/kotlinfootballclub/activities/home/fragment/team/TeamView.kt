package com.khilman.kotlinfootballclub.activities.home.fragment.team

import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.leagues.LeaguesItem

interface TeamView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String)
    fun showLeagues(leagues: List<LeaguesItem>)
    fun showTeams(teams: List<TeamsItem>)
}