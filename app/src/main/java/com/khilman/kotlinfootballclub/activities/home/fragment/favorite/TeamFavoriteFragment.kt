package com.khilman.kotlinfootballclub.activities.home.fragment.favorite


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.activities.home.adapter.TeamFavAdapter
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import kotlinx.android.synthetic.main.fragment_team_favorite.view.*
import org.jetbrains.anko.support.v4.toast

class TeamFavoriteFragment : Fragment(), TeamFavoriteView  {

    val presenter = TeamFavoritePresenter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_team_favorite, container, false)


        //minta ke presenter
        presenter.getFavTeams(context)
        return v
    }

    override fun showLoading() {

    }

    override fun hideLoading() {
    }

    override fun showMessage(message: String) {
    }

    override fun showEvents(events: List<TeamsItem>) {
        toast("Hey masok sini")
        //TODO: Prosesnya ke-stop sampe sini :(
        view?.rv_favorite_teams?.adapter = TeamFavAdapter(context, mutableListOf())
        view?.rv_favorite_teams?.layoutManager = LinearLayoutManager(context)
    }

}
