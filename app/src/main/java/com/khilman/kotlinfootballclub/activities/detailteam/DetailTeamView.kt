package com.khilman.kotlinfootballclub.activities.detailteam

import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem

interface DetailTeamView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(message: String)
    fun showOverview(teamDetail: TeamsItem)
    fun showTeamList(teamPlayers: List<PlayerItem?>)
}