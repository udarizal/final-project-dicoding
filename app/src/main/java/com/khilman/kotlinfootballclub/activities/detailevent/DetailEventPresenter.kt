package com.khilman.kotlinfootballclub.activities.detailevent

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.khilman.kotlinfootballclub.database.MatchEvent
import com.khilman.kotlinfootballclub.database.database
import com.khilman.kotlinfootballclub.network.response.EventsItem
import com.khilman.kotlinfootballclub.network.response.TeamsItem
import com.khilman.kotlinfootballclub.utils.MyConts
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DetailEventPresenter(val view: DetailEventView, val interactor: DetailEventInteractor) :
    DetailEventInteractor.OnGetEventFinished {

    fun showEvents(eventId: String) {
        if (eventId.isNotEmpty()) {
            view.showLoading()
            interactor.getDetailEvent(eventId, this)
        }
    }

    fun showDetailTeam(teamId: String, whichTeam: String) {
        if (teamId.isNotEmpty()) {
            interactor.getDetailTeam(teamId, whichTeam, this)
        }
    }

    fun addToFavorite(
        context: Context,
        dataEvent: EventsItem
    ){
        try {
            context?.database.use {
                insert(
                    MatchEvent.TABLE_FAVORITE_MATCH, MatchEvent.MATCH_ID to dataEvent.idEvent.toString(),
                    MatchEvent.MATCH_DATE to dataEvent.dateEvent,
                    MatchEvent.HOME_TEAM to dataEvent.strHomeTeam,
                    MatchEvent.HOME_SCORE to dataEvent.intHomeScore.toString(),
                    MatchEvent.AWAY_TEAM to dataEvent.strAwayTeam.toString(),
                    MatchEvent.AWAY_SCORE to dataEvent.intAwayScore.toString()
                )
            }
            view?.showMessage("Ditambahkan ke favorite")

        } catch (e: SQLiteConstraintException){
            view?.showMessage(e.localizedMessage)
        }
    }

    fun removeFromFavorite(
        context: Context,
        idEvent: String
    ){
        try {
            context?.database.use {
                delete(MatchEvent.TABLE_FAVORITE_MATCH, "(${MatchEvent.MATCH_ID} = {id})", "id" to idEvent)
            }
            view?.showMessage("FavoriteTeam dihapus")
        } catch (e: SQLiteConstraintException) {
            view?.showMessage(e.localizedMessage)
        }
    }

    fun isFavorite (
        context: Context,
        idEvent: String
    ): Boolean {
        var isFavorite = false
        context?.database.use {
            val result = select(MatchEvent.TABLE_FAVORITE_MATCH)
                .whereArgs("(${MatchEvent.MATCH_ID} = {id})", "id" to idEvent)
            val favorite = result.parseList(classParser<MatchEvent>())
            isFavorite = favorite.isNotEmpty()
        }
        return isFavorite
    }

    override fun onTeamFound(team: TeamsItem, whichTeam: String) {
        if (whichTeam.equals(MyConts.TEAM_HOME)) {
            view?.showDetailHomeTeam(team)
        } else {
            view?.showDetailAwayTeam(team)
        }
    }

    override fun onError(message: String) {
        view?.apply {
            showMessage(message)
            hideLoading()
        }
    }

    override fun onSuccess(events: List<EventsItem>) {
        view?.apply {
            showDetailEvent(events)
            hideLoading()
        }
    }
}