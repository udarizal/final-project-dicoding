package com.khilman.kotlinfootballclub.activities.detailteam.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khilman.kotlinfootballclub.R
import com.khilman.kotlinfootballclub.network.response.player.PlayerItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.player_item_list.view.*

class PlayerAdapter(val context: Context?, val players: List<PlayerItem?>) : RecyclerView.Adapter<PlayerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): PlayerAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.player_item_list, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = players.size

    override fun onBindViewHolder(holder: PlayerAdapter.ViewHolder, position: Int) {
        val data = players[position]
        data?.let { holder.bind(it) }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: PlayerItem) {
            itemView.tvClubName.text = data.strPlayer
            itemView.tb_player_position.text = data.strPosition
            Picasso.with(context).load(data.strCutout).into(itemView.ivClubImage)
        }
    }
}