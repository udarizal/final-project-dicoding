package com.khilman.kotlinfootballclub

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.khilman.kotlinfootballclub.R.id.*
import com.khilman.kotlinfootballclub.activities.home.MatchScheduleActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import com.khilman.kotlinfootballclub.utils.ResourceIdlingResource

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {
    @Rule
    @JvmField var activityRule = ActivityTestRule(MatchScheduleActivity::class.java)

    @Test
    fun testRecyclerViewBehaviour(){
        IdlingRegistry.getInstance().register(ResourceIdlingResource(activityRule.activity))
        onView(withId(recyclerView)).check(matches(isDisplayed()))
        onView(withId(recyclerView)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(3))
        onView(withId(recyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))

        IdlingRegistry.getInstance().register(ResourceIdlingResource(activityRule.activity))

        onView(withId(addToFavorite)).check(matches(isDisplayed()))
        onView(withId(addToFavorite)).perform(click())
        onView(withText("Ditambahkan ke favorite")).check(matches(isDisplayed()))

        pressBack()

        onView(withId(favorite_match)).check(matches(isDisplayed()))
        onView(withId(favorite_match)).perform(click())

        IdlingRegistry.getInstance().register(ResourceIdlingResource(activityRule.activity))

        onView(withId(recyclerView)).check(matches(isDisplayed()))
        onView(withId(recyclerView)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(recyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        IdlingRegistry.getInstance().register(ResourceIdlingResource(activityRule.activity))

        onView(withId(addToFavorite)).check(matches(isDisplayed()))
        onView(withId(addToFavorite)).perform(click())
        onView(withText("FavoriteTeam dihapus")).check(matches(isDisplayed()))

        pressBack()
    }

    @Test
    fun testBottomNavigation()  {
        onView(withId(prev_match)).check(matches(isDisplayed()))
        onView(withId(prev_match)).perform(click())

        onView(withId(next_match)).check(matches(isDisplayed()))
        onView(withId(next_match)).perform(click())

        onView(withId(favorite_match)).check(matches(isDisplayed()))
        onView(withId(favorite_match)).perform(click())
    }



}